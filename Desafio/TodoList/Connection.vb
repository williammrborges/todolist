﻿Imports Npgsql
Imports System.Data

Public Class Connection
    Private Property conn As New NpgsqlConnection
    Public Sub IniciaConexao()
        Dim conexao As New Uri("postgres://eggfgvezgsjenv:352e07be7d0a7476141a01d0a462f59671cd5a66b1bdd4a1650597319c189092@ec2-18-235-97-230.compute-1.amazonaws.com:5432/dt3b0ft7obpfj")
        Dim UserInfo() As String = conexao.UserInfo.Split(":")

        Dim builder As New NpgsqlConnectionStringBuilder With
        {
            .Host = conexao.Host,
            .Port = conexao.Port,
            .Username = UserInfo(0),
            .Password = UserInfo(1),
            .Database = conexao.LocalPath.TrimStart("/"),
            .SslMode = SslMode.Require,
            .TrustServerCertificate = True
        }

        conn = New NpgsqlConnection(builder.ToString())
        conn.Open()

    End Sub

    Public Sub FechaConexao()
        conn.Close()
    End Sub

    Friend Function RealizaQuery(sql As String) As DataTable
        Try

            Dim DataAdapter As Npgsql.NpgsqlDataAdapter = New NpgsqlDataAdapter(sql, conn)
            Dim DataSet As DataSet = New DataSet()

            DataAdapter.Fill(DataSet)

            Return DataSet.Tables(0)
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

End Class
