﻿Public Class Projeto
    Public Property CodProjeto As Integer
    Public Property DescProjeto As String
    Public Property DtInicio As Date
    Public Property DtFim As Date?
    Public Property PercCompleto As Decimal
    Public Property Atrasado As Boolean
End Class
