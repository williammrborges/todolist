﻿Public Class Tarefa
    Public Property CodTarefa As Integer
    Public Property CodProjeto As Integer
    Public Property DescProjeto As String
    Public Property DescAtividade As String
    Public Property DtInicio As Date
    Public Property DtFim As Date?
    Public Property Finalizada As Boolean
End Class
