﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TelaPrincipal
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txNomeProjeto = New System.Windows.Forms.TextBox()
        Me.btnAlt = New System.Windows.Forms.Button()
        Me.dgProjetos = New System.Windows.Forms.DataGridView()
        Me.ProjetoBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.btnDel = New System.Windows.Forms.Button()
        Me.dtInicio = New System.Windows.Forms.DateTimePicker()
        Me.dtFim = New System.Windows.Forms.DateTimePicker()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnTarefas = New System.Windows.Forms.Button()
        Me.ProjetoBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProjetoBindingSource2 = New System.Windows.Forms.BindingSource(Me.components)
        Me.CodProjetoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescProjetoDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DtInicioDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DtFimDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PercCompleto = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Atrasado = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        CType(Me.dgProjetos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProjetoBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProjetoBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProjetoBindingSource2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txNomeProjeto
        '
        Me.txNomeProjeto.Location = New System.Drawing.Point(74, 15)
        Me.txNomeProjeto.Name = "txNomeProjeto"
        Me.txNomeProjeto.Size = New System.Drawing.Size(484, 20)
        Me.txNomeProjeto.TabIndex = 0
        '
        'btnAlt
        '
        Me.btnAlt.Location = New System.Drawing.Point(678, 8)
        Me.btnAlt.Name = "btnAlt"
        Me.btnAlt.Size = New System.Drawing.Size(98, 23)
        Me.btnAlt.TabIndex = 1
        Me.btnAlt.Text = "Alterar"
        Me.btnAlt.UseVisualStyleBackColor = True
        '
        'dgProjetos
        '
        Me.dgProjetos.AutoGenerateColumns = False
        Me.dgProjetos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgProjetos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CodProjetoDataGridViewTextBoxColumn, Me.DescProjetoDataGridViewTextBoxColumn, Me.DtInicioDataGridViewTextBoxColumn, Me.DtFimDataGridViewTextBoxColumn, Me.PercCompleto, Me.Atrasado})
        Me.dgProjetos.DataSource = Me.ProjetoBindingSource2
        Me.dgProjetos.Location = New System.Drawing.Point(28, 80)
        Me.dgProjetos.Name = "dgProjetos"
        Me.dgProjetos.Size = New System.Drawing.Size(748, 267)
        Me.dgProjetos.TabIndex = 2
        '
        'ProjetoBindingSource
        '
        Me.ProjetoBindingSource.DataSource = GetType(todolist.Projeto)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(25, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(43, 13)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Projeto:"
        '
        'btnDel
        '
        Me.btnDel.Location = New System.Drawing.Point(574, 51)
        Me.btnDel.Name = "btnDel"
        Me.btnDel.Size = New System.Drawing.Size(98, 23)
        Me.btnDel.TabIndex = 4
        Me.btnDel.Text = "Excluir Projeto"
        Me.btnDel.UseVisualStyleBackColor = True
        '
        'dtInicio
        '
        Me.dtInicio.Location = New System.Drawing.Point(74, 54)
        Me.dtInicio.Name = "dtInicio"
        Me.dtInicio.Size = New System.Drawing.Size(235, 20)
        Me.dtInicio.TabIndex = 5
        '
        'dtFim
        '
        Me.dtFim.CausesValidation = False
        Me.dtFim.Location = New System.Drawing.Point(315, 54)
        Me.dtFim.Name = "dtFim"
        Me.dtFim.Size = New System.Drawing.Size(243, 20)
        Me.dtFim.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(71, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(75, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Data de Início"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(315, 38)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 13)
        Me.Label3.TabIndex = 8
        Me.Label3.Text = "Data Final:"
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(574, 8)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(98, 23)
        Me.btnAdd.TabIndex = 9
        Me.btnAdd.Text = "Adicionar"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnTarefas
        '
        Me.btnTarefas.Location = New System.Drawing.Point(602, 366)
        Me.btnTarefas.Name = "btnTarefas"
        Me.btnTarefas.Size = New System.Drawing.Size(174, 31)
        Me.btnTarefas.TabIndex = 10
        Me.btnTarefas.Text = "Tarefas do Projeto"
        Me.btnTarefas.UseVisualStyleBackColor = True
        '
        'ProjetoBindingSource1
        '
        Me.ProjetoBindingSource1.DataSource = GetType(todolist.Projeto)
        '
        'ProjetoBindingSource2
        '
        Me.ProjetoBindingSource2.DataSource = GetType(todolist.Projeto)
        '
        'CodProjetoDataGridViewTextBoxColumn
        '
        Me.CodProjetoDataGridViewTextBoxColumn.DataPropertyName = "CodProjeto"
        Me.CodProjetoDataGridViewTextBoxColumn.HeaderText = "Código do Projeto"
        Me.CodProjetoDataGridViewTextBoxColumn.Name = "CodProjetoDataGridViewTextBoxColumn"
        Me.CodProjetoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DescProjetoDataGridViewTextBoxColumn
        '
        Me.DescProjetoDataGridViewTextBoxColumn.DataPropertyName = "DescProjeto"
        Me.DescProjetoDataGridViewTextBoxColumn.HeaderText = "Descrição do Projeto"
        Me.DescProjetoDataGridViewTextBoxColumn.Name = "DescProjetoDataGridViewTextBoxColumn"
        Me.DescProjetoDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DtInicioDataGridViewTextBoxColumn
        '
        Me.DtInicioDataGridViewTextBoxColumn.DataPropertyName = "DtInicio"
        Me.DtInicioDataGridViewTextBoxColumn.HeaderText = "Data de Início"
        Me.DtInicioDataGridViewTextBoxColumn.Name = "DtInicioDataGridViewTextBoxColumn"
        Me.DtInicioDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DtFimDataGridViewTextBoxColumn
        '
        Me.DtFimDataGridViewTextBoxColumn.DataPropertyName = "DtFim"
        Me.DtFimDataGridViewTextBoxColumn.HeaderText = "Data de Fim"
        Me.DtFimDataGridViewTextBoxColumn.Name = "DtFimDataGridViewTextBoxColumn"
        Me.DtFimDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PercCompleto
        '
        Me.PercCompleto.DataPropertyName = "PercCompleto"
        Me.PercCompleto.HeaderText = "% Completo"
        Me.PercCompleto.Name = "PercCompleto"
        Me.PercCompleto.ReadOnly = True
        '
        'Atrasado
        '
        Me.Atrasado.DataPropertyName = "Atrasado"
        Me.Atrasado.HeaderText = "Irá atrasar?"
        Me.Atrasado.Name = "Atrasado"
        Me.Atrasado.ReadOnly = True
        '
        'TelaPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.btnTarefas)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dtFim)
        Me.Controls.Add(Me.dtInicio)
        Me.Controls.Add(Me.btnDel)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dgProjetos)
        Me.Controls.Add(Me.btnAlt)
        Me.Controls.Add(Me.txNomeProjeto)
        Me.Name = "TelaPrincipal"
        Me.Text = "Projetos"
        CType(Me.dgProjetos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProjetoBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProjetoBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProjetoBindingSource2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txNomeProjeto As TextBox
    Friend WithEvents btnAlt As Button
    Friend WithEvents dgProjetos As DataGridView
    Friend WithEvents Label1 As Label
    Friend WithEvents btnDel As Button
    Friend WithEvents dtInicio As DateTimePicker
    Friend WithEvents dtFim As DateTimePicker
    Friend WithEvents ProjetoBindingSource As BindingSource
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents btnAdd As Button
    Friend WithEvents btnTarefas As Button
    Friend WithEvents CodProjetoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DescProjetoDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DtInicioDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DtFimDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PercCompleto As DataGridViewTextBoxColumn
    Friend WithEvents Atrasado As DataGridViewCheckBoxColumn
    Friend WithEvents ProjetoBindingSource2 As BindingSource
    Friend WithEvents ProjetoBindingSource1 As BindingSource
End Class
