﻿Public Class TelaPrincipal
    Private Property Connection As New Connection
    Private Property ListaProjetos As New List(Of Projeto)
    Private Property LinhaSelecionada As New Projeto

    Private Sub BuscaInicial()
        Dim projetos = Connection.RealizaQuery(" SELECT *,
                                                   Coalesce((SELECT Round(( ( calc_perc.finalizada * 100 ) /
                                                                          calc_perc.total ), 2)
                                                             FROM   (SELECT Sum(CASE
                                                                                  WHEN a2.atv_finalizada THEN 1
                                                                                  ELSE 0
                                                                                end) AS finalizada,
                                                                            Count(*) AS total
                                                                     FROM   atividades a2
                                                                     WHERE  atv_cod_projeto = p.prj_cod_projeto) AS
                                                                    calc_perc), 0)
                                                           AS perc,
                                                   ( CASE
                                                       WHEN (SELECT atv_dt_fim
                                                             FROM   atividades a3
                                                             WHERE  atv_cod_projeto = p.prj_cod_projeto
                                                             ORDER  BY atv_dt_fim DESC
                                                             LIMIT  1) > p.prj_dt_fim THEN 1
                                                       ELSE 0
                                                     end ) AS atrasada
                                            FROM   projetos p  ")
        ListaProjetos = New List(Of Projeto)
        For Each reg In projetos.Rows
            Dim linha As New Projeto With {.CodProjeto = reg(0), .DescProjeto = reg(1)}
            Dim dataInicio, dataFim As Date
            Date.TryParse(reg(2).ToString(), dataInicio)
            Date.TryParse(reg(3).ToString(), dataFim)
            linha.DtInicio = dataInicio
            linha.DtFim = dataFim
            linha.PercCompleto = reg(4)
            linha.Atrasado = reg(5)
            ListaProjetos.Add(linha)
        Next
        dgProjetos.DataSource = ListaProjetos
    End Sub

    Private Sub TelaPrincipal_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Connection.IniciaConexao()
        BuscaInicial()
    End Sub

    Private Sub dgProjetos_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgProjetos.CellClick
        If dgProjetos.SelectedCells.Count <= 0 Then Exit Sub

        Dim rowIndex = dgProjetos.SelectedCells(0).RowIndex
        Dim linha = dgProjetos.Rows(rowIndex)

        Dim codProjeto = linha.Cells(0).Value

        If codProjeto = LinhaSelecionada.CodProjeto Then Exit Sub

        Dim linhaProjeto = ListaProjetos.Where(Function(r) r.CodProjeto = codProjeto).FirstOrDefault()

        If linhaProjeto IsNot Nothing Then
            LinhaSelecionada = linhaProjeto

            txNomeProjeto.Text = LinhaSelecionada.DescProjeto
            dtInicio.Value = LinhaSelecionada.DtInicio
            If LinhaSelecionada.DtFim <> New Date() Then dtFim.Value = LinhaSelecionada.DtFim
        End If
    End Sub

    Private Sub btnDel_Click(sender As Object, e As EventArgs) Handles btnDel.Click
        Dim result = MessageBox.Show("Confirma a Exclusão?", "Exclusão", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then
            Dim sql = Connection.RealizaQuery(String.Format("DELETE FROM projetos WHERE prj_cod_projeto = {0}", LinhaSelecionada.CodProjeto))
            ZeraCampos()
            BuscaInicial()
        End If
    End Sub

    Private Sub btnAlt_Click(sender As Object, e As EventArgs) Handles btnAlt.Click
        Dim result = MessageBox.Show("Confirma a Alteração?", "Alteração", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then
            Dim sql = String.Format("UPDATE projetos SET prj_nm_projeto = '{0}', prj_dt_inicio = '{1}', prj_dt_fim = '{2}' WHERE prj_cod_projeto = {3}", txNomeProjeto.Text, dtInicio.Value.ToString("yyyy-MM-dd"), dtFim.Value.ToString("yyyy-MM-dd"), LinhaSelecionada.CodProjeto)
            Dim projetoRow = Connection.RealizaQuery(sql)
            ZeraCampos()
            BuscaInicial()
        End If
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        If LinhaSelecionada.CodProjeto > 0 Then
            btnAlt.PerformClick()
            Exit Sub
        End If

        Dim sql = String.Format("INSERT INTO projetos (prj_nm_projeto, prj_dt_inicio, prj_dt_fim) VALUES ('{0}', '{1}', '{2}')", txNomeProjeto.Text, dtInicio.Value.ToString("yyyy-MM-dd"), dtFim.Value.ToString("yyyy-MM-dd"))
        Dim projetoRow = Connection.RealizaQuery(sql)
        ZeraCampos()
        BuscaInicial()
    End Sub

    Private Sub ZeraCampos()
        LinhaSelecionada = New Projeto
        txNomeProjeto.Text = ""
        dtInicio.Value = Now()
        dtFim.Value = Now()
    End Sub

    Private Sub btnTarefas_Click(sender As Object, e As EventArgs) Handles btnTarefas.Click

        If LinhaSelecionada.CodProjeto <= 0 Then
            MessageBox.Show("Selecione o projeto para verificar as tarefas.")
            Exit Sub
        End If
        Using telaTarefa = New TelaTarefas
            telaTarefa.AbrirTela(LinhaSelecionada.CodProjeto, Connection)
        End Using
    End Sub

    Private Sub TelaPrincipal_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        Connection.FechaConexao()
    End Sub
End Class
