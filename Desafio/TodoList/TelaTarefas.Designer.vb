﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class TelaTarefas
    Inherits System.Windows.Forms.Form

    'Descartar substituições de formulário para limpar a lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Exigido pelo Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'OBSERVAÇÃO: o procedimento a seguir é exigido pelo Windows Form Designer
    'Pode ser modificado usando o Windows Form Designer.  
    'Não o modifique usando o editor de códigos.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.dgTarefas = New System.Windows.Forms.DataGridView()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.dtFim = New System.Windows.Forms.DateTimePicker()
        Me.dtInicio = New System.Windows.Forms.DateTimePicker()
        Me.btnDel = New System.Windows.Forms.Button()
        Me.lbTarefa = New System.Windows.Forms.Label()
        Me.btnAlt = New System.Windows.Forms.Button()
        Me.txNomeTarefa = New System.Windows.Forms.TextBox()
        Me.ckFinalizada = New System.Windows.Forms.CheckBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lbCodProjeto = New System.Windows.Forms.Label()
        Me.lbDescProjeto = New System.Windows.Forms.Label()
        Me.CodTarefaDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescAtividadeDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DtInicioDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DtFimDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FinalizadaDataGridViewCheckBoxColumn = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.TarefaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        CType(Me.dgTarefas, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TarefaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgTarefas
        '
        Me.dgTarefas.AutoGenerateColumns = False
        Me.dgTarefas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgTarefas.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.CodTarefaDataGridViewTextBoxColumn, Me.DescAtividadeDataGridViewTextBoxColumn, Me.DtInicioDataGridViewTextBoxColumn, Me.DtFimDataGridViewTextBoxColumn, Me.FinalizadaDataGridViewCheckBoxColumn})
        Me.dgTarefas.DataSource = Me.TarefaBindingSource
        Me.dgTarefas.Location = New System.Drawing.Point(12, 127)
        Me.dgTarefas.Name = "dgTarefas"
        Me.dgTarefas.Size = New System.Drawing.Size(764, 297)
        Me.dgTarefas.TabIndex = 0
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(564, 16)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(98, 23)
        Me.btnAdd.TabIndex = 18
        Me.btnAdd.Text = "Adicionar"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(305, 46)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 13)
        Me.Label3.TabIndex = 17
        Me.Label3.Text = "Data Final:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(61, 46)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(75, 13)
        Me.Label2.TabIndex = 16
        Me.Label2.Text = "Data de Início"
        '
        'dtFim
        '
        Me.dtFim.CausesValidation = False
        Me.dtFim.Location = New System.Drawing.Point(305, 62)
        Me.dtFim.Name = "dtFim"
        Me.dtFim.Size = New System.Drawing.Size(243, 20)
        Me.dtFim.TabIndex = 15
        '
        'dtInicio
        '
        Me.dtInicio.Location = New System.Drawing.Point(64, 62)
        Me.dtInicio.Name = "dtInicio"
        Me.dtInicio.Size = New System.Drawing.Size(235, 20)
        Me.dtInicio.TabIndex = 14
        '
        'btnDel
        '
        Me.btnDel.Location = New System.Drawing.Point(564, 59)
        Me.btnDel.Name = "btnDel"
        Me.btnDel.Size = New System.Drawing.Size(98, 23)
        Me.btnDel.TabIndex = 13
        Me.btnDel.Text = "Excluir Tarefa"
        Me.btnDel.UseVisualStyleBackColor = True
        '
        'lbTarefa
        '
        Me.lbTarefa.AutoSize = True
        Me.lbTarefa.Location = New System.Drawing.Point(15, 26)
        Me.lbTarefa.Name = "lbTarefa"
        Me.lbTarefa.Size = New System.Drawing.Size(41, 13)
        Me.lbTarefa.TabIndex = 12
        Me.lbTarefa.Text = "Tarefa:"
        '
        'btnAlt
        '
        Me.btnAlt.Location = New System.Drawing.Point(668, 16)
        Me.btnAlt.Name = "btnAlt"
        Me.btnAlt.Size = New System.Drawing.Size(98, 23)
        Me.btnAlt.TabIndex = 11
        Me.btnAlt.Text = "Alterar"
        Me.btnAlt.UseVisualStyleBackColor = True
        '
        'txNomeTarefa
        '
        Me.txNomeTarefa.Location = New System.Drawing.Point(64, 23)
        Me.txNomeTarefa.Name = "txNomeTarefa"
        Me.txNomeTarefa.Size = New System.Drawing.Size(484, 20)
        Me.txNomeTarefa.TabIndex = 10
        '
        'ckFinalizada
        '
        Me.ckFinalizada.AutoSize = True
        Me.ckFinalizada.Location = New System.Drawing.Point(64, 88)
        Me.ckFinalizada.Name = "ckFinalizada"
        Me.ckFinalizada.Size = New System.Drawing.Size(113, 17)
        Me.ckFinalizada.TabIndex = 19
        Me.ckFinalizada.Text = "Tarefa Finalizada?"
        Me.ckFinalizada.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 111)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 13)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "Cód. Projeto:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(154, 111)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(74, 13)
        Me.Label4.TabIndex = 21
        Me.Label4.Text = "Desc. Projeto:"
        '
        'lbCodProjeto
        '
        Me.lbCodProjeto.AutoSize = True
        Me.lbCodProjeto.Location = New System.Drawing.Point(80, 111)
        Me.lbCodProjeto.Name = "lbCodProjeto"
        Me.lbCodProjeto.Size = New System.Drawing.Size(0, 13)
        Me.lbCodProjeto.TabIndex = 22
        '
        'lbDescProjeto
        '
        Me.lbDescProjeto.AutoSize = True
        Me.lbDescProjeto.Location = New System.Drawing.Point(231, 111)
        Me.lbDescProjeto.Name = "lbDescProjeto"
        Me.lbDescProjeto.Size = New System.Drawing.Size(0, 13)
        Me.lbDescProjeto.TabIndex = 23
        '
        'CodTarefaDataGridViewTextBoxColumn
        '
        Me.CodTarefaDataGridViewTextBoxColumn.DataPropertyName = "CodTarefa"
        Me.CodTarefaDataGridViewTextBoxColumn.HeaderText = "Código da Tarefa"
        Me.CodTarefaDataGridViewTextBoxColumn.Name = "CodTarefaDataGridViewTextBoxColumn"
        Me.CodTarefaDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DescAtividadeDataGridViewTextBoxColumn
        '
        Me.DescAtividadeDataGridViewTextBoxColumn.DataPropertyName = "DescAtividade"
        Me.DescAtividadeDataGridViewTextBoxColumn.HeaderText = "Descrição Tarefa"
        Me.DescAtividadeDataGridViewTextBoxColumn.Name = "DescAtividadeDataGridViewTextBoxColumn"
        Me.DescAtividadeDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DtInicioDataGridViewTextBoxColumn
        '
        Me.DtInicioDataGridViewTextBoxColumn.DataPropertyName = "DtInicio"
        Me.DtInicioDataGridViewTextBoxColumn.HeaderText = "Data de Início"
        Me.DtInicioDataGridViewTextBoxColumn.Name = "DtInicioDataGridViewTextBoxColumn"
        Me.DtInicioDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DtFimDataGridViewTextBoxColumn
        '
        Me.DtFimDataGridViewTextBoxColumn.DataPropertyName = "DtFim"
        Me.DtFimDataGridViewTextBoxColumn.HeaderText = "Data Fim"
        Me.DtFimDataGridViewTextBoxColumn.Name = "DtFimDataGridViewTextBoxColumn"
        Me.DtFimDataGridViewTextBoxColumn.ReadOnly = True
        '
        'FinalizadaDataGridViewCheckBoxColumn
        '
        Me.FinalizadaDataGridViewCheckBoxColumn.DataPropertyName = "Finalizada"
        Me.FinalizadaDataGridViewCheckBoxColumn.HeaderText = "Finalizada"
        Me.FinalizadaDataGridViewCheckBoxColumn.Name = "FinalizadaDataGridViewCheckBoxColumn"
        Me.FinalizadaDataGridViewCheckBoxColumn.ReadOnly = True
        '
        'TarefaBindingSource
        '
        Me.TarefaBindingSource.DataSource = GetType(todolist.Tarefa)
        '
        'TelaTarefas
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.lbDescProjeto)
        Me.Controls.Add(Me.lbCodProjeto)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ckFinalizada)
        Me.Controls.Add(Me.btnAdd)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.dtFim)
        Me.Controls.Add(Me.dtInicio)
        Me.Controls.Add(Me.btnDel)
        Me.Controls.Add(Me.lbTarefa)
        Me.Controls.Add(Me.btnAlt)
        Me.Controls.Add(Me.txNomeTarefa)
        Me.Controls.Add(Me.dgTarefas)
        Me.Name = "TelaTarefas"
        Me.Text = "TelaTarefas"
        CType(Me.dgTarefas, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TarefaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents dgTarefas As DataGridView
    Friend WithEvents btnAdd As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents dtFim As DateTimePicker
    Friend WithEvents dtInicio As DateTimePicker
    Friend WithEvents btnDel As Button
    Friend WithEvents lbTarefa As Label
    Friend WithEvents btnAlt As Button
    Friend WithEvents txNomeTarefa As TextBox
    Friend WithEvents ckFinalizada As CheckBox
    Friend WithEvents TarefaBindingSource As BindingSource
    Friend WithEvents Label1 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents lbCodProjeto As Label
    Friend WithEvents lbDescProjeto As Label
    Friend WithEvents CodTarefaDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DescAtividadeDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DtInicioDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents DtFimDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents FinalizadaDataGridViewCheckBoxColumn As DataGridViewCheckBoxColumn
End Class
