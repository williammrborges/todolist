﻿Public Class TelaTarefas
    Private Property Connection As New Connection
    Private Property Projeto As New Projeto
    Private Property TarefaSelecionada As New Tarefa
    Private Property ListaTarefa As New List(Of Tarefa)
    Public Sub AbrirTela(codProjeto As Integer, conn As Connection)
        Connection = conn
        Dim consulta = Connection.RealizaQuery(String.Format("SELECT * FROM projetos WHERE prj_cod_projeto = {0}", codProjeto)).Rows()
        For Each reg In consulta
            Dim dtInicio As Date
            Dim dtFim As Date
            Date.TryParse(reg(2), dtInicio)
            Date.TryParse(reg(3), dtFim)

            Projeto = New Projeto With {
                .CodProjeto = reg(0),
                .DescProjeto = reg(1),
                .DtInicio = dtInicio,
                .DtFim = dtFim
            }
        Next

        lbCodProjeto.Text = Projeto.CodProjeto
        lbDescProjeto.Text = Projeto.DescProjeto

        Me.ShowDialog()
    End Sub

    Private Sub BuscaInicial()

        Dim consulta = Connection.RealizaQuery(String.Format("SELECT * FROM atividades WHERE atv_cod_projeto = {0}", Projeto.CodProjeto)).Rows()
        ListaTarefa = New List(Of Tarefa)
        For Each reg In consulta
            Dim dtInicio As Date
            Dim dtFim As Date
            Date.TryParse(reg(2), dtInicio)
            Date.TryParse(reg(3), dtFim)
            Dim tarefa = New Tarefa With {
                .CodTarefa = reg(0),
                .DescAtividade = reg(1),
                .DtInicio = dtInicio,
                .DtFim = dtFim,
                .Finalizada = reg(4),
                .CodProjeto = Projeto.CodProjeto,
                .DescProjeto = Projeto.DescProjeto
            }

            ListaTarefa.Add(tarefa)
        Next

        dgTarefas.DataSource = ListaTarefa
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        If TarefaSelecionada.CodTarefa > 0 Then
            btnAlt.PerformClick()
            Exit Sub
        End If

        Dim sql = String.Format("INSERT INTO atividades (atv_desc_atividade, atv_dt_inicio, atv_dt_fim, atv_finalizada, atv_cod_projeto)
                                VALUES('{0}', '{1}', '{2}', {3}, {4})",
                                txNomeTarefa.Text, dtInicio.Value.ToString("yyyy-MM-dd"), dtFim.Value.ToString("yyyy-MM-dd"), ckFinalizada.Checked, Projeto.CodProjeto)

        Dim projetoRow = Connection.RealizaQuery(sql)
        ZeraCampos()
        BuscaInicial()
    End Sub

    Private Sub btnAlt_Click(sender As Object, e As EventArgs) Handles btnAlt.Click
        Dim result = MessageBox.Show("Confirma a Alteração?", "Alteração", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then
            Dim sql = String.Format("UPDATE public.atividades
                                        SET atv_desc_atividade='{0}', atv_dt_inicio='{1}', atv_dt_fim='{2}', atv_finalizada={3}
                                        WHERE atv_cod_atividade={4}",
                                        txNomeTarefa.Text, dtInicio.Value.ToString("yyyy-MM-dd"), dtFim.Value.ToString("yyyy-MM-dd"), ckFinalizada.Checked, TarefaSelecionada.CodTarefa)
            Dim projetoRow = Connection.RealizaQuery(sql)
            ZeraCampos()
            BuscaInicial()
        End If
    End Sub

    Private Sub btnDel_Click(sender As Object, e As EventArgs) Handles btnDel.Click
        Dim result = MessageBox.Show("Confirma a Exclusão?", "Exclusão", MessageBoxButtons.YesNo)

        If result = DialogResult.Yes Then
            Dim sql = Connection.RealizaQuery(String.Format("DELETE FROM atividades WHERE atv_cod_atividade = {0}", TarefaSelecionada.CodTarefa))
            ZeraCampos()
            BuscaInicial()
        End If
    End Sub

    Private Sub ZeraCampos()
        TarefaSelecionada = New Tarefa
        txNomeTarefa.Text = ""
        dtInicio.Value = Now()
        dtFim.Value = Now()
        ckFinalizada.Checked = False
    End Sub

    Private Sub TelaTarefas_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        BuscaInicial()
    End Sub

    Private Sub dgTarefas_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgTarefas.CellClick
        If dgTarefas.SelectedCells.Count <= 0 Then Exit Sub

        Dim rowIndex = dgTarefas.SelectedCells(0).RowIndex
        Dim linha = dgTarefas.Rows(rowIndex)

        Dim codTarefa = linha.Cells(0).Value

        If codTarefa = TarefaSelecionada.CodTarefa Then Exit Sub

        Dim linhaTarefa = ListaTarefa.Where(Function(r) r.CodTarefa = codTarefa).FirstOrDefault()

        If linhaTarefa IsNot Nothing Then
            TarefaSelecionada = linhaTarefa

            txNomeTarefa.Text = linhaTarefa.DescAtividade
            dtInicio.Value = linhaTarefa.DtInicio
            If linhaTarefa.DtFim <> New Date() Then dtFim.Value = linhaTarefa.DtFim
            ckFinalizada.Checked = linhaTarefa.Finalizada
        End If
    End Sub
End Class